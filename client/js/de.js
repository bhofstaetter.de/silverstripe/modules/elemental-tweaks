// See https://github.com/silverstripe/cow for details
if (typeof(ss) === 'undefined' || typeof(ss.i18n) === 'undefined') {
    if (typeof(console) !== 'undefined') { // eslint-disable-line no-console
        console.error('Class ss.i18n not defined');  // eslint-disable-line no-console
    }
} else {
    ss.i18n.addDictionary('de', {
        "ElementArchiveAction.ARCHIVE": "Archivieren",
        "ElementArchiveAction.DUPLICATE": "Kopieren",
        "ElementArchiveAction.ARCHIVE_PERMISSION_DENY": "Konnte nicht archiviert werden, fehlende Berechtigungen",
        "ElementArchiveAction.DUPLICATE_PERMISSION_DENY": "Konnte nicht kopiert werden, fehlende Berechtigungen",
        "ElementPublishAction.PUBLISH_PERMISSION_DENY": "Konnte nicht veröffentlicht werden, fehlende Berechtigungen",
        "ElementUnpublishAction.UNPUBLISH_PERMISSION_DENY": "Veröffentlichung konnte nicht zurückgenommen werden, fehlende Berechtigungen",
        "ElementalElement.CHILD_RENDERING_ERROR": "Etwas hat nicht funktioniert. Bitte probieren Sie die Seite zu speichern und neu zu laden",
        "HistoricElementView.VIEW_BLOCK_HISTORY": "Verlauf",
        "ElementArchiveAction.UNPUBLISH": "Veröffentlichung zurücknehmen",
        "ElementPublishAction.PUBLISH": "Veröffentlichen"
    });
}
