<?php

namespace bhofstaetter\ElementalTweaks;

use SilverStripe\ORM\DataExtension;

class EnabledForAccordionElementExtension extends DataExtension
{
    use MeWithContextTrait;
}
