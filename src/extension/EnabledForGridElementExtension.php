<?php

namespace bhofstaetter\ElementalTweaks;

use SilverStripe\ORM\DataExtension;

class EnabledForGridElementExtension extends DataExtension
{
    use MeWithContextTrait;
}
