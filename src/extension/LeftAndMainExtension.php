<?php

namespace bhofstaetter\ElementalTweaks;

use SilverStripe\Core\Extension;
use SilverStripe\View\Requirements;

class LeftAndMainExtension extends Extension
{
    public function onAfterInit() {
        Requirements::javascript('bhofstaetter/elemental-tweaks:client/js/de.js');
    }
}
