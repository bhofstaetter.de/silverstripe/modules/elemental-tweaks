<?php

namespace bhofstaetter\ElementalTweaks;

use bhofstaetter\FrameworkTweaks\BooleanDropdownField;
use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HeaderField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\Queries\SQLUpdate;
use SilverStripe\Versioned\Versioned;

class BaseElementExtension extends DataExtension
{
    protected bool $parentLastEditedUpdated = false;

    private static array $db = [
        'CenterName' => "Enum('0,1',0)",
        'NameTag' => "Enum('h1,h2,h3,h4,h5,h6','h2')",
        'Separator' => "Enum('none,bottom','none')",
    ];

    public function updateFormScaffolder($fs, $owner)
    {
        $fs->restrictFields = [
            'HTML',
            'Style', // todo: enable!
        ];

        $fs->includeRelations = false;
        $fs->tabbed = true;
    }

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldsToTab('Root.Settings', [
            HeaderField::create('SettingsHeader', 'Allgemeine Einstellungen'),
            DropdownField::create('NameTag', 'Art der Überschrift', [
                'h1' => 'H1',
                'h2' => 'H2',
                'h3' => 'H3',
                'h4' => 'H4',
                'h5' => 'H5',
                'h6' => 'H6',
            ], 'h2'),
            BooleanDropdownField::create('CenterName', 'Überschrift zentrieren'),
            DropdownField::create('Separator', 'Abgrenzung anzeigen', [
                'none' => 'nein',
                'bottom' => 'unter dem Block',
            ], 'none')
        ]);
    }

    public function onAfterArchive()
    {
        $this->updateParentLastEdited();
    }

    public function onAfterDelete()
    {
        $this->updateParentLastEdited();
    }

    public function onAfterPublish()
    {
        $this->updateParentLastEdited();
    }

    public function onAfterUnpublish()
    {
        $this->updateParentLastEdited();
    }

    public function onAfterWrite()
    {
        $this->updateParentLastEdited();
    }

    public function updateParentLastEdited()
    {
        if ($this->parentLastEditedUpdated) {
            return;
        }

        $parent = $this->owner->getPage();

        if ($parent) {
            $now = date('Y-m-d H:i:s');

            if (is_subclass_of($parent, SiteTree::class)) {
                $tableName = SiteTree::config()->get('table_name');
            } else {
                $tableName = $parent->config()->get('table_name');
            }

            try {
                SQLUpdate::create($tableName)
                    ->addWhere(['ID' => $parent->ID])
                    ->addAssignments(['LastEdited' => $now])
                    ->execute();
            } catch (\Exception $ignore) {
            }

            if ($parent->hasExtension(Versioned::class)) {
                try {
                    SQLUpdate::create($tableName . '_Live')
                        ->addWhere(['ID' => $parent->ID])
                        ->addAssignments(['LastEdited' => $now])
                        ->execute();
                } catch (\Exception $ignore) {
                }
            }

            $this->parentLastEditedUpdated = true;
        }
    }

    public function CacheKey(): string
    {
        $parts = [
            str_replace('\\', '-', $this->owner->ClassName),
            $this->owner->ID,
            $this->owner->LastEdited,
        ];

        $this->owner->extend('updateCacheKey', $parts);

        return implode('_', $parts);
    }
}
