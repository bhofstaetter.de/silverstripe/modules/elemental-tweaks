<?php

namespace bhofstaetter\ElementalTweaks;

class TextElement extends GCOElement
{
    private static string $singular_name = 'Text';
    private static string $plural_name = 'Texte';
    private static string $table_name = 'ET_TextElement';
    private static string $description = 'Text';

    private static bool $content_enabled = true;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        // ...

        return $fields;
    }
}
