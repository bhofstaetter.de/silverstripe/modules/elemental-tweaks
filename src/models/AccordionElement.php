<?php

namespace bhofstaetter\ElementalTweaks;

use bhofstaetter\GCO\ContentObject;
use SilverStripe\Core\ClassInfo;

class AccordionElement extends GCOElement
{
    private static string $singular_name = 'Accordion';
    private static string $plural_name = 'Accordions';
    private static string $table_name = 'ET_AccordionElement';
    private static string $description = 'Accordion';
    private static bool $inline_editable = false;

    private static bool $links_enabled = true;
    private static bool $content_objects_enabled = true;

    private static int $links_max = 1;

    public function updateAllowedContentObjectsClasses(&$classes)
    {
        $contentObjectChildren = ClassInfo::subclassesFor(ContentObject::class, false);
        $otherClasses = [];

        foreach ($contentObjectChildren as $contentObjectChild) {
            if ($contentObjectChild::singleton()->hasExtension(EnabledForAccordionElementExtension::class)) {
                $otherClasses[] = $contentObjectChild;
            }
        }

        $classes = array_merge(
            ClassInfo::subclassesFor(AccordionItem::class),
            $otherClasses,
        );
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        // ...

        return $fields;
    }
}
