<?php

namespace bhofstaetter\ElementalTweaks;

use bhofstaetter\GCO\ContentObject;

class AccordionItem extends ContentObject
{
    private static $singular_name = 'Accordion item';
    private static $plural_name = 'Accordion items';
    private static $table_name = 'ET_AccordionItem';

    private static bool $show_name_enabled = true;
    private static bool $content_enabled = true;
    private static bool $icon_enabled = true;
    private static bool $images_enabled = true;
    private static int $images_max = 1;

    private static bool $show_name_default = true;


    private static $belongs_many_many = [
        'Accordions' => AccordionElement::class,
    ];
}
