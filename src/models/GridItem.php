<?php

namespace bhofstaetter\ElementalTweaks;

use bhofstaetter\GCO\ContentObject;

class GridItem extends ContentObject
{
    private static $singular_name = 'Grid item';
    private static $plural_name = 'Grid items';
    private static $table_name = 'ET_GridItem';

    private static bool $show_name_enabled = true;
    private static bool $content_enabled = true;
    private static bool $icon_enabled = true;
    private static bool $links_enabled = true;
    private static int $links_max = 1;

    private static bool $show_name_default = true;


    private static $belongs_many_many = [
        'Grids' => GridElement::class,
    ];
}
