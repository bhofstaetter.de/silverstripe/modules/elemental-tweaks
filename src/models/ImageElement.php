<?php

namespace bhofstaetter\ElementalTweaks;

class ImageElement extends GCOElement
{
    private static string $singular_name = 'Image';
    private static string $table_name = 'ET_ImageElement';
    private static string $description = 'Image';

    private static bool $images_enabled = true;
    private static int $images_max = 1;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        // ...

        return $fields;
    }
}
