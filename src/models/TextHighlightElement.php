<?php

namespace bhofstaetter\ElementalTweaks;

use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class TextHighlightElement extends GCOElement
{
    private static string $singular_name = 'Text Highlight';
    private static string $plural_name = 'Texte Highlights';
    private static string $table_name = 'ET_TextHighlightElement';
    private static string $description = 'Text';

    private static bool $content_enabled = true;
    private static bool $icon_enabled = true;

    private static $db = [
        'Content2' => 'HTMLText',
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Main', [
            HTMLEditorField::create('Content2', 'Inhalt 2'),
        ]);

        // ...

        return $fields;
    }
}
