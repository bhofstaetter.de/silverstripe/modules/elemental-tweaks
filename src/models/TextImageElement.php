<?php

namespace bhofstaetter\ElementalTweaks;

use SilverStripe\Forms\DropdownField;

class TextImageElement extends GCOElement
{
    private static string $singular_name = 'Text & Image';
    private static string $table_name = 'ET_TextImageElement';
    private static string $description = 'Text & Image';

    private static bool $images_enabled = true;
    private static bool $content_enabled = true;
    private static int $images_max = 1;

    private static $db = [
        'ImagePosition' => "Enum('left,right','right')",
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Settings', [
            DropdownField::create('ImagePosition', 'Bildposition', $this->dbObject('ImagePosition')->niceEnumValues())
        ]);

        // ...

        return $fields;
    }
}
