<?php

namespace bhofstaetter\ElementalTweaks;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Control\Controller;
use SilverStripe\Forms\DropdownField;

class GCOElement extends BaseElement
{
    private static string $table_name = 'GCOElement';
    private static bool $show_name_enabled = true;

    private static $db = [
        'ShowName' => "Enum('yes,no','yes')",
    ];

    public function getType(): string
    {
        $default = $this->i18n_singular_name() ?: 'Block';

        return _t($this->ClassName . '.BlockType', $default);
    }

    public function skipGCOValidation(&$skipValidation)
    {
        $skipValidation = $this->skipValidationOnCreate();
    }

    public function skipRequiredFieldsValidation(&$skipValidation)
    {
        $skipValidation = $this->skipValidationOnCreate();
    }

    private function skipValidationOnCreate(): bool
    {
        $ctrl = Controller::curr();

        if (
            $ctrl->getAction() === 'index'
            && $ctrl->getRequest()->getURL() === 'admin/graphql'
            && str_contains($ctrl->getRequest()->getBody(), 'AddElementToArea')
        ) {
            return true;
        }

        return false;
    }

    public function getTitle()
    {
        return $this->Name;
    }

    public function getCMSFields()
    {
        $fields = $this->getBasicCMSFields(true, true); // todo: automatic title/name field => winning field config if both exists

        $this->extend('updateCMSFields', $fields);

        $styles = $this->config()->get('styles');
        if ($styles && count($styles ?? []) > 0) {
            $styleDropdown = DropdownField::create('Style', _t(BaseElement::class.'.STYLE', 'Style variation'), $styles);
            $styleDropdown->setEmptyString(_t(__CLASS__.'.CUSTOM_STYLES', 'Select a style..'));
            $fields->addFieldToTab('Root.Settings', $styleDropdown, '');

        }

        $fields->dataFieldByName('ShowName')->setSource($this->dbObject('ShowName')->NiceEnumValues());

        return $fields;
    }
}
