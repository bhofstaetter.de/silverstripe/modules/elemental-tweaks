<?php

namespace bhofstaetter\ElementalTweaks;

use bhofstaetter\GCO\ContentObject;
use SilverStripe\Core\ClassInfo;

class GridElement extends GCOElement
{
    private static string $singular_name = 'Grid';
    private static string $plural_name = 'Grids';
    private static string $table_name = 'ET_GridElement';
    private static string $description = 'Grid';
    private static bool $inline_editable = false;

    private static bool $links_enabled = true;
    private static bool $content_objects_enabled = true;

    private static int $links_max = 1;

    public function updateAllowedContentObjectsClasses(&$classes)
    {
        $contentObjectChildren = ClassInfo::subclassesFor(ContentObject::class, false);
        $otherClasses = [];

        foreach ($contentObjectChildren as $contentObjectChild) {
            if ($contentObjectChild::singleton()->hasExtension(EnabledForGridElementExtension::class)) {
                $otherClasses[] = $contentObjectChild;
            }
        }

        $classes = array_merge(
            ClassInfo::subclassesFor(GridItem::class),
            $otherClasses,
        );
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        // ...

        return $fields;
    }
}
