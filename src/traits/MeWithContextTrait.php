<?php

namespace bhofstaetter\ElementalTweaks;

use DNADesign\Elemental\Models\BaseElement;

trait MeWithContextTrait
{
    public function MeWithContext($parentID)
    {
        $context = BaseElement::get()->byID($parentID);

        return $this->owner
            ->customise([
                'Context' => $context
            ])
            ->renderWith([
                $this->owner->template,
                $this->owner->ClassName,
                GridItem::class,
                'type' => 'Includes'
            ]);
    }
}
